﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour
{

    public float Speed;

    private Rigidbody2D rbody;
    private Animator anim;

	// Use this for initialization
	void Start ()
	{
	    rbody = GetComponent<Rigidbody2D>();
	    anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update ()
	{
	    float horiz = Input.GetAxisRaw("Horizontal");
	    float vert = Input.GetAxisRaw("Vertical");
	    Vector2 movement = new Vector2(horiz, vert);

	    if (movement != Vector2.zero)
	    {
	        anim.SetBool("iswalking", true);
            anim.SetFloat("input_x", movement.x);
            anim.SetFloat("input_y", movement.y);
	    }
	    else
	    {
	        anim.SetBool("iswalking", false);
	    }

        rbody.MovePosition(rbody.position + movement * Time.deltaTime * Speed);
	}
}
